﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using GlobalUtils;

namespace PQEMonitor
{
    /// <summary>
    /// Implements the connection logic for the socket client.
    /// </summary>
    internal sealed class SocketClient : IDisposable
    {        
        /// <summary>
        /// Invoke feeder notification
        /// </summary>
        public event Action<string, string> FeederNotificationEvent;

        public event Action<string, string, string> FeederNoQuotesEvent;
        public event Action<string> ResetNoQuotesEvent;

        private readonly int BUFFER_SIZE = 8196;

        /// <summary>
        /// The socket used to send/receive messages.
        /// </summary>
        private Socket clientSocket;

        /// <summary>
        /// Flag for connected socket.
        /// </summary>
        private Boolean connected = false;

        /// <summary>
        /// Listener endpoint.
        /// </summary>
        private IPEndPoint hostEndPoint;

        /// <summary>
        /// Signals a connection.
        /// </summary>
        private AutoResetEvent autoConnectEvent = new AutoResetEvent(false);

        /// <summary>
        /// Signals a dis-connection.
        /// </summary>
        private AutoResetEvent autoDisconnectEvent;

        private string ID;

        char[] charsToTrim = { '\0' };
        char[] sep = { '\r', '\n', ' ' };
        string left = string.Empty;


        MongoDBMng mongo;
        Dictionary<string, string> symbolsMap;
        Publisher<Quote> publisher;
        Dictionary<int, List<FeedSession>> FeedSessions;

        System.Timers.Timer QuotesCheck;
        int GotQuotes;
        bool SendResetNoQuotesEvent;

        /// <summary>
        /// Create an uninitialized client instance.  
        /// To start the send/receive processing
        /// call the Connect method followed by SendReceive method.
        /// </summary>
        /// <param name="hostName">Name of the host where the listener is running.</param>
        /// <param name="port">Number of the TCP port from the listener.</param>
        internal SocketClient(string PublisherAddress, Dictionary<int, List<FeedSession>> FeedSessions, String hostName, Int32 port, string ID, MongoDBMng mongo, Dictionary<string, string> symbolsMap, AutoResetEvent autoDisconnectEvent = null)
        {
            //quote = new Quote();

            this.autoDisconnectEvent = autoDisconnectEvent;
            this.ID = ID;
            this.symbolsMap = symbolsMap;
            this.mongo = mongo;            
            this.FeedSessions = FeedSessions;

            GotQuotes = 0;
            SendResetNoQuotesEvent = true;

            QuotesCheck = new System.Timers.Timer();
            QuotesCheck.SynchronizingObject = null;
            QuotesCheck.Interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CheckQuotesIntervalMS"]);
            QuotesCheck.Elapsed += QuotesCheck_Elapsed;

            publisher = new Publisher<Quote>(PublisherAddress, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NetMQHWM"]));

            /*
            // Get host related information.
            IPHostEntry host = Dns.GetHostEntry(hostName);

            // Addres of the host.
            IPAddress[] addressList = host.AddressList;

            // Instantiates the endpoint and socket.
            this.hostEndPoint = new IPEndPoint(addressList[addressList.Length - 1], port);   
            */

            this.hostEndPoint = new IPEndPoint(IPAddress.Parse(hostName), port);

            //this.clientSocket = new Socket(this.hostEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        private void QuotesCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            QuotesCheck.Stop();

            if (Interlocked.Exchange(ref GotQuotes, 0) == 0 && FeederNotificationEvent != null)
            {
                if (Common.IsFeedInSession(FeedSessions))
                {                    
                    if (FeederNoQuotesEvent != null)
                        FeederNoQuotesEvent(ID, hostEndPoint.Address.ToString(), hostEndPoint.Port.ToString());

                    if (autoDisconnectEvent is AutoResetEvent)
                    {
                        if (!autoDisconnectEvent.SafeWaitHandle.IsClosed)
                            autoDisconnectEvent.Set();
                        return;
                    }
                }
            }

            if (ResetNoQuotesEvent != null && SendResetNoQuotesEvent)
            {
                SendResetNoQuotesEvent = false;
                ResetNoQuotesEvent(ID);
            }

            QuotesCheck.Start();
        }

        /// <summary>
        /// Connect to the host.
        /// </summary>
        /// <param name="Timeout">How much to wait in miliseconds for connect event.</param>
        /// <returns>True if connection has succeded, else false.</returns>
        internal bool Connect(int Timeout/*, int ConnectFailRetryOutputNotification*/)
        {
            GotQuotes = 0;

            this.clientSocket = new Socket(this.hostEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            SocketAsyncEventArgs connectArgs = new SocketAsyncEventArgs();

            connectArgs.UserToken = this.clientSocket;
            connectArgs.RemoteEndPoint = this.hostEndPoint;
            connectArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnConnect);
            
            clientSocket.ConnectAsync(connectArgs);
            autoConnectEvent.WaitOne(Timeout, true);
            connectArgs.Completed -= OnConnect;

            SocketError errorCode = connectArgs.SocketError;
            if (!this.connected || errorCode != SocketError.Success)
            {                
                //throw new SocketException((Int32)errorCode);

                /*
                if (((++Counter % ConnectFailRetryOutputNotification) == 0) && FeederNotificationEvent != null)
                {
                    string Subject = "Feeder Down Notification";
                    string Body = "PQE Monitor Fail connecting to <b>" + ID + "</b><br/><br/>IP: " + hostEndPoint.Address.ToString() + "<br/>Port: " + hostEndPoint.Port.ToString() + "<br/><br/>";
                    Body += "Please check connectivity parameters and check connection to server.";
                    FeederNotificationEvent(Subject, Body);
                }
                */

                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Disconnect from the host.
        /// </summary>
        internal void Disconnect()
        {
            //clientSocket.Disconnect(false);
            try
            {
                clientSocket.Close();
            }
            catch { }
            try
            {
                clientSocket.Dispose();
            }
            catch { }

        }

        private void OnConnect(object sender, SocketAsyncEventArgs e)
        {            
            // Set the flag for socket connected.
            this.connected = (e.SocketError == SocketError.Success);

            // Signals the end of connection.
            autoConnectEvent.Set();
        }
        
        private void OnReceive(object sender, SocketAsyncEventArgs e)
        {
            Socket s = e.UserToken as Socket;
            //System.Diagnostics.Debug.WriteLine("Connected: " + s.Connected.ToString());
                        
            if (!s.Connected || e.SocketError != SocketError.Success)
            {               
                this.ProcessError(e);
                return;
            }

            //if (s.Connected == true && e.SocketError == SocketError.Success)
            {
                string receivestr = Encoding.ASCII.GetString(e.Buffer, e.Offset, e.BytesTransferred);
                int epos = receivestr.LastIndexOf("\r\n");
                if (epos != -1)
                {
                    Interlocked.Exchange(ref GotQuotes, 1);
                    string dataFromClient = left + receivestr.Substring(0, epos);

                    left = receivestr.Substring(epos + 2);

                    //Console.Write(dataFromClient + Environment.NewLine);

                    //parse data...
                                        
                    string[] dat = dataFromClient.Split(sep);
                    try
                    {
                        for (int k = 0, i = 1; k < dat.Length; i++)
                        {
                            Quote quote = new Quote();
                            quote.BrandName = ID;
                            symbolsMap.TryGetValue(dat[k], out quote.SymbolName);
                            quote.ServerTime = DateTime.ParseExact(dat[k + 1], "dd/MM/yyyy-H:m:s.fff", System.Globalization.CultureInfo.InvariantCulture);
                            quote.LocalTime = DateTime.Now;
                            quote.Bid = Convert.ToDouble(dat[k + 2]);
                            quote.Ask = Convert.ToDouble(dat[k + 3]);

                            mongo.Update(quote);
                            publisher.Publish(quote);
                            //blockingQueue.Enqueue(quote);

                            k = i * 5;
                        }
                    }
                    catch { }
                }
                else
                    left += receivestr;

                if (!clientSocket.ReceiveAsync(e))
                    OnReceive(null, e);
            }
          
            /*else
            {
                this.ProcessError(e);
            }*/
        }

        private void OnSend(object sender, SocketAsyncEventArgs e)
        {

        }

        /// <summary>
        /// Close socket in case of failure and throws a SockeException according to the SocketError.
        /// </summary>
        /// <param name="e">SocketAsyncEventArg associated with the failed operation.</param>
        private void ProcessError(SocketAsyncEventArgs e)
        {
            QuotesCheck.Stop();

            Socket s = e.UserToken as Socket;
            try
            {
                s.Close();
            }
            catch { }
            try
            {
                s.Dispose();
            }
            catch { }
          
            if (autoDisconnectEvent is AutoResetEvent)
            {                
                if(!autoDisconnectEvent.SafeWaitHandle.IsClosed)
                    autoDisconnectEvent.Set();
            }

            e.Completed -= new EventHandler<SocketAsyncEventArgs>(OnReceive);

            // Throw the SocketException
            //throw new SocketException((Int32)e.SocketError);
        }

        /// <summary>
        /// Start receive messages from the host.
        /// </summary>        
        /// <returns>Message sent by the host.</returns>
        internal bool StartReceive()
        {
            if (this.connected)
            {
                byte[] buffer = new byte[BUFFER_SIZE];
                // Prepare arguments for send/receive operation.
                SocketAsyncEventArgs completeArgs = new SocketAsyncEventArgs();
                completeArgs.SetBuffer(buffer, 0, BUFFER_SIZE);
                completeArgs.UserToken = this.clientSocket;
                completeArgs.RemoteEndPoint = this.hostEndPoint;
                completeArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnReceive);
                                
                Task.Factory.StartNew(() => clientSocket.ReceiveAsync(completeArgs));

                QuotesCheck.Start();

                return true;
            }
            else
            {
                //throw new SocketException((Int32)SocketError.NotConnected);                
                return false;
            }

        }

        public bool Login(string Username, string Password, string SuccessString)
        {
            bool ret = false;
            if (this.connected)
            {
                byte[] b = new byte[32];
                int len = clientSocket.Receive(b);
                if (len == 0)
                    return ret;
                string line = System.Text.Encoding.ASCII.GetString(b, 0, len);
                len = clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(Username + "\r\n"));

                b = new byte[32];
                len = clientSocket.Receive(b);
                if (len == 0)
                    return ret;
                line = System.Text.Encoding.ASCII.GetString(b, 0, len);
                len = clientSocket.Send(System.Text.Encoding.ASCII.GetBytes(Password + "\r\n"));

                b = new byte[32];
                len = clientSocket.Receive(b);
                if (len == 0)
                    return ret;
                line = System.Text.Encoding.ASCII.GetString(b, 0, len);

                if (line.StartsWith(SuccessString))
                    ret = true;
            }

            return ret;
        }

        #region IDisposable Members

        /// <summary>
        /// Disposes the instance of SocketClient.
        /// </summary>
        public void Dispose()
        {
            publisher.Dispose();

            autoConnectEvent.Close();
            try
            {
                autoConnectEvent.Dispose();
            }
            catch { }

            QuotesCheck.Stop();
            QuotesCheck.Close();
            try
            {
                QuotesCheck.Dispose();
            }
            catch { }

            Disconnect();
        }

        #endregion
    }
}
