﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using GlobalUtils;

namespace PQEMonitor
{
    class Program
    {
        /*private static void getitem(BlockingQueue<Quote> q)
        {
            while(true)
            {
                Quote i = q.Dequeue();
                Console.Write(i.Ask + " ");
            }
        }

        private static void additem(BlockingQueue<Quote> q, int id)
        {
            double i = 1.0;
            while(true)
            {
                Quote qq = new Quote();
                qq.Ask = i++;
                qq.BrandName = "fsdfds";
                q.Enqueue(qq);
                Thread.Sleep(5);
            }
        }*/

        static Mailer mail;
        static ConcurrentDictionary<string, int> ConnectFailCounter;
        static ConcurrentDictionary<string, int> NoQuotesCounter;
        static void Main(string[] args)
        {
            /*BlockingQueue<Quote> q = new BlockingQueue<Quote>();

            var t2 = new Thread(() => getitem(q));
            t2.Start();

            var t1 = new Thread(() => additem(q, 1));
            t1.Start();

            var t3 = new Thread(() => additem(q, 2));
            t3.Start();

            Console.ReadKey();*/


            try
            {
                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["BaseLogsDirectory"]);
            }
            catch { }

            GlobalUtils.Tracer.GetTracer().Info("Monitor Started");

            ConnectFailCounter = new ConcurrentDictionary<string, int>();
            NoQuotesCounter = new ConcurrentDictionary<string, int>();

            //Publisher<Quote> publisher = new Publisher<Quote>(ConfigurationManager.AppSettings["NetMQAddress"], Convert.ToInt32(ConfigurationManager.AppSettings["NetMQHWM"]));

            mail = new Mailer(Convert.ToInt32(ConfigurationManager.AppSettings["SmtpObjectPoolSize"]), ConfigurationManager.AppSettings["MailSmtpServer"], Convert.ToInt32(ConfigurationManager.AppSettings["MailSmtpPort"]),
                ((ConfigurationManager.AppSettings["MailUseDefaultCredentials"] == "true") ? (true) : (false)), ConfigurationManager.AppSettings["MailUsername"], ConfigurationManager.AppSettings["MailPassword"],
                ((ConfigurationManager.AppSettings["MailEnableSSL"] == "true") ? (true) : (false)), ConfigurationManager.AppSettings["MailDeliveryMethod"], Convert.ToInt32(ConfigurationManager.AppSettings["SmtpConnectionTimeoutMS"]),
                Convert.ToInt32(ConfigurationManager.AppSettings["SendMailRetryTimes"]), ((ConfigurationManager.AppSettings["IgnoreCetificateValidationOnSendMail"] == "true") ? (true) : (false)));

            MongoDBMng mongo = new MongoDBMng(ConfigurationManager.AppSettings["MongoConnStr"], ConfigurationManager.AppSettings["MongoDatabaseName"]);
            mongo.CreateDatabase(ConfigurationManager.AppSettings["MongoDatabaseName"]);
            mongo.ConnectCollection("snapshot", true);
            
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("feeds.xml");
            XmlNodeList nodes = xDoc.SelectNodes("//feeds/feed");

            foreach (XmlNode node in nodes)
            {
                ConnectFailCounter.TryAdd(node.SelectSingleNode("name").InnerText, 0);
                var t = new Thread(() => BuildSymbolsList(mongo, node));
                t.Start();
            }

            Console.ReadKey();            
        }

        private static void BuildSymbolsList(MongoDBMng mongo, XmlNode node)
        {
            List<string> ignoreSymbols = ConfigurationManager.AppSettings["IgnoreSymbols"].Split(';').ToList();

            Dictionary<string, string> symbolsMap = new Dictionary<string, string>();

            XmlDocument xDoc = new XmlDocument();

            #region Regular symbols

            xDoc.Load(node.SelectSingleNode("symbolsPath").InnerText + "Streams.xml");

            XmlNodeList nodes = xDoc.SelectNodes("//Streams/Stream");

            if (node.SelectSingleNode("saveOutputName").InnerText == "true")
            {
                foreach (XmlNode n in nodes)
                {
                    if (ignoreSymbols.Contains(n.Attributes["Symbol"].Value))
                        continue;

                    string outputName = (string.IsNullOrWhiteSpace(n.Attributes["OutputName"].Value)) ? (n.Attributes["Symbol"].Value) : (n.Attributes["OutputName"].Value);
                    try
                    {
                        symbolsMap.Add(outputName, outputName);
                    }
                    catch { }
                }
            }
            else
            {
                foreach (XmlNode n in nodes)
                {
                    if (ignoreSymbols.Contains(n.Attributes["Symbol"].Value))
                        continue;

                    string outputName = (string.IsNullOrWhiteSpace(n.Attributes["OutputName"].Value)) ? (n.Attributes["Symbol"].Value) : (n.Attributes["OutputName"].Value);
                    try
                    {
                        symbolsMap.Add(outputName, n.Attributes["Symbol"].Value);
                    }
                    catch { }
                }
            }
            #endregion

            #region Synthetic symbols
            xDoc.Load(node.SelectSingleNode("symbolsPath").InnerText + "SyntheticStreams.xml");
            nodes = xDoc.SelectNodes("//Streams/Stream");
            foreach (XmlNode n in nodes)
            {
                string outputName = (string.IsNullOrWhiteSpace(n.Attributes["OutputName"].Value)) ? (n.Attributes["Symbol"].Value) : (n.Attributes["OutputName"].Value);
                try
                {
                    symbolsMap.Add(outputName, outputName);
                }
                catch { }                
            }
            #endregion

            System.Collections.IEnumerator e = symbolsMap.GetEnumerator();
            while (e.MoveNext())
            {
                KeyValuePair<string, string> s = (KeyValuePair<string, string>)e.Current;

                Quote quote = new Quote()
                {
                    BrandName = node.SelectSingleNode("name").InnerText,
                    SymbolName = s.Value
                };
              
                /*string res = mongo.InsertWithResult(quote);
                if(!string.IsNullOrWhiteSpace(res))
                    Tracer.GetTracer().Error("Fail adding " + s.Value + " for " + node.SelectSingleNode("name").InnerText + ". Error: " + res);*/
            }

            mongo.ConnectCollection("snapshot", false);
            
            var t = new Thread(() => FeedProcessor(null, mongo, node, symbolsMap));
            t.Start();
        }

        private static Dictionary<int, List<FeedSession>> ParseSessions(string Sessions, string FeederName)
        {
            Dictionary<int, List<FeedSession>> FeedSessions = new Dictionary<int, List<FeedSession>>();

            if (!string.IsNullOrWhiteSpace(Sessions))
            {                
                try
                {
                    string[] s1 = Sessions.Split(';');
                    for (int i = 0; i < s1.Length; i++)
                    {
                        string[] s2 = s1[i].Split('@');
                        int day = Convert.ToInt32(s2[0]);

                        //00:00-23:59
                        string[] s3 = s2[1].Split('-');

                        FeedSession feedSession = new FeedSession();

                        int sh = Convert.ToInt32(s3[0].Split(':')[0]);
                        int sm = Convert.ToInt32(s3[0].Split(':')[1]);
                        feedSession.Start = new TimeSpan(sh, sm, 0);

                        int eh = Convert.ToInt32(s3[1].Split(':')[0]);
                        int em = Convert.ToInt32(s3[1].Split(':')[1]);
                        feedSession.End = new TimeSpan(eh, em, 59);

                        if (FeedSessions.ContainsKey(day))
                        {
                            FeedSessions[day].Add(feedSession);
                        }
                        else
                        {
                            List<FeedSession> FeedSessionCollection = new List<FeedSession>();
                            FeedSessionCollection.Add(feedSession);
                            FeedSessions.Add(day, FeedSessionCollection);
                        }
                    }
                }
                catch (System.Exception ex)
                {                    
                    GlobalUtils.Tracer.GetTracer().Error("Fail load feed sessions for " + FeederName + " - " + ex.Message);                    
                }
            }
            
            return FeedSessions;
        }
        
        private static void FeedProcessor(Dictionary<int, List<FeedSession>> FeedSessions, MongoDBMng mongo, XmlNode node, Dictionary<string, string> symbolsMap)
        {
            SocketClient socketClient = default(SocketClient);
            AutoResetEvent wait = new AutoResetEvent(false);
            int retryTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["ConnectRetryDelayMSec"]);
            int ConnectFailRetryOutputNotification = Convert.ToInt32(ConfigurationManager.AppSettings["ConnectFailRetryOutputNotification"]);

            try
            {
                if (FeedSessions == null)
                {                    
                    FeedSessions = ParseSessions(node.SelectSingleNode("sessions").InnerText, node.SelectSingleNode("name").InnerText);
                }

                GlobalUtils.Tracer.GetTracer().Info("Connecting to " + node.SelectSingleNode("name").InnerText);

                int timeout = Convert.ToInt32(ConfigurationManager.AppSettings["ConnectTimeoutMSec"]);  
                //int ConnectFailRetryOutputNotification = Convert.ToInt32(ConfigurationManager.AppSettings["ConnectFailRetryOutputNotification"]);

                AutoResetEvent autoDisconnectEvent = new AutoResetEvent(false);
                             
                using (socketClient = new SocketClient(node.SelectSingleNode("publisher").InnerText, FeedSessions, node.SelectSingleNode("ip").InnerText, Convert.ToInt32(node.SelectSingleNode("port").InnerText), node.SelectSingleNode("name").InnerText, mongo, symbolsMap, autoDisconnectEvent))
                {                    
                    socketClient.FeederNotificationEvent += SocketClient_FeederNotificationEvent;
                    socketClient.ResetNoQuotesEvent += SocketClient_ResetNoQuotesEvent;
                    //while (true)
                    {
                        if (socketClient.Connect(timeout/*, ConnectFailRetryOutputNotification*/))
                        {
                            if(node.SelectSingleNode("authenticate").InnerText == "true" && !socketClient.Login(node.SelectSingleNode("username").InnerText, node.SelectSingleNode("password").InnerText, node.SelectSingleNode("successString").InnerText))
                            {
                                GlobalUtils.Tracer.GetTracer().Error("Feed authentication [" + node.SelectSingleNode("name").InnerText + "] failed");
                            }
                            else if (socketClient.StartReceive())
                            {
                                socketClient.FeederNoQuotesEvent += SocketClient_FeederNoQuotesEvent;
                                GlobalUtils.Tracer.GetTracer().Info("Connected to " + node.SelectSingleNode("name").InnerText);
                                ConnectFailCounter[node.SelectSingleNode("name").InnerText] = 0;
                                autoDisconnectEvent.WaitOne();                                
                            }
                        }

                        //socketClient.Disconnect();
                        autoDisconnectEvent.Close();
                        try
                        {
                            autoDisconnectEvent.Dispose();
                        }
                        catch { }
                        GlobalUtils.Tracer.GetTracer().Info("Disconnected from " + node.SelectSingleNode("name").InnerText + "[" + node.SelectSingleNode("ip").InnerText + "]");

                        //wait.WaitOne(retryTimeout);                        
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalUtils.Tracer.GetTracer().Error("Feed exception [" + node.SelectSingleNode("name").InnerText + "]: " + ex.Message);               
            }
            finally
            {
                if (socketClient is SocketClient)
                {
                    socketClient.FeederNotificationEvent -= SocketClient_FeederNotificationEvent;
                    socketClient.FeederNoQuotesEvent -= SocketClient_FeederNoQuotesEvent;
                    socketClient.ResetNoQuotesEvent -= SocketClient_ResetNoQuotesEvent;
                }
            }
            
            ConnectFailCounter[node.SelectSingleNode("name").InnerText]++;
            if (((++ConnectFailCounter[node.SelectSingleNode("name").InnerText] % ConnectFailRetryOutputNotification) == 0))
            {
                if (Common.IsFeedInSession(FeedSessions))
                {
                    string Subject = "Feeder Down Notification";
                    string Body = "PQE Monitor Fail connecting to <b>" + node.SelectSingleNode("name").InnerText + "</b><br/><br/>IP: " + node.SelectSingleNode("ip").InnerText + "<br/>Port: " + node.SelectSingleNode("port").InnerText + "<br/><br/>";
                    Body += "Please check connectivity parameters and check connection to server.";
                    SocketClient_FeederNotificationEvent(Subject, Body);
                }
            }

            wait.WaitOne(retryTimeout);
            wait.Close();
            try
            {
                wait.Dispose();
            }
            catch { }

            var t = new Thread(() => FeedProcessor(FeedSessions, mongo, node, symbolsMap));
            t.Start();
        }

        private static void SocketClient_ResetNoQuotesEvent(string obj)
        {
            NoQuotesCounter.AddOrUpdate(obj, 0, (k, v) => v = 0);
        }

        private static void SocketClient_FeederNoQuotesEvent(string obj1, string obj2, string obj3)
        {
            if(NoQuotesCounter.ContainsKey(obj1))
            {
                NoQuotesCounter[obj1]++;
                if(NoQuotesCounter[obj1] >= 2)
                {
                    NoQuotesCounter[obj1] = 0;

                    string Subject = "No Quotes Alert";
                    string Body = "PQE Monitor stop recieving quotes from <b>" + obj1 + "</b> (altough the socket is connected)<br/><br/>Reconnecting to " + obj2 + ":" + obj3 + "<br/><br/>";
                    Body += "If the problem persist and you keep getting these kind of mails in short intervals, please connect to <b>" + obj1 + "</b> engine via PQE GUI terminal and check its up and send quotes.</b>";
                    Body += "You may also check that the sessions for this feed are correct in Feeds.xml file.<br/>";
                    Body += "In case sessions are correct please connect to " + obj2 + " server and restart <b>" + obj1 + "</b> engine process!";
                    SocketClient_FeederNotificationEvent(Subject, Body);
                }
            }
            else
            {
                NoQuotesCounter.TryAdd(obj1, 1);
            }
        }

        private static void SocketClient_FeederNotificationEvent(string arg1, string arg2)
        {
            MailData mailData = new MailData();
            mailData.Subject = arg1;
            mailData.Body = arg2;

            mailData.MailDisplayName = ConfigurationManager.AppSettings["MailDisplayName"];
            mailData.MailFromAddr = ConfigurationManager.AppSettings["MailFromAddr"];

            mailData.To = ConfigurationManager.AppSettings["SentTo"];
            mailData.Bcc = ConfigurationManager.AppSettings["Bcc"];
            mailData.Cc = ConfigurationManager.AppSettings["Cc"];
            
            mailData.ResendCounter = 0;

            mail.SendMail(mailData);
        }
    }
}
