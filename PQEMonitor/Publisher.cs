﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlobalUtils;
using Newtonsoft.Json;

namespace PQEMonitor
{
    public class Publisher<T> : NetMQBase
    {
        object obj = null;
        public Publisher(string Address, int HighWaterMark):base(SocketType.PUB, Address, HighWaterMark, "")
        {
            obj = new object();
        }

        public void Publish(T item)
        {
            PublishMessage(JsonConvert.SerializeObject(item));
        }

        public void PublishWithLock(T item)
        {
            lock (obj)
            {
                PublishMessage(JsonConvert.SerializeObject(item));
            }
        }
    }
}
