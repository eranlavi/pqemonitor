﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace GlobalUtils
{
    /// <summary>
    /// A generic object pool. Use this class to store and get objects that are creation expensive.
    /// </summary>
    /// <typeparam name="T">Generic object</typeparam>
    public class ObjectPool<T>
    {
        private ConcurrentBag<T> _objects;
        private int MaxItems;
        private Action<T> _cleanUp;

        /// <summary>
        /// Class CTor.
        /// </summary>
        /// <param name="cleanUp">Delegate method that will be called uppon calling PutObject method with 'Release' = true.</param>
        /// <param name="MaxItems">Maximum object the pool will store.</param>
        public ObjectPool(Action<T> cleanUp, int MaxItems = 25)
        {
            this.MaxItems = MaxItems;

            _objects = new ConcurrentBag<T>();

            _cleanUp = cleanUp;
        }

        /// <summary>
        /// Get object from pool. This method is thread safe.
        /// </summary>
        /// <returns>An object. In case object is empty it will return null in case the pool is storing objects or 0 in case pool stores primitive types.</returns>
        public T GetObject()
        {
            T item;
            if (_objects.TryTake(out item))
                return item;
            else
                return default(T);
        }

        /// <summary>
        /// Insert object into pool. This method is thread safe.
        /// </summary>
        /// <param name="item">Object type to insert into the pool.</param>
        /// <param name="Release">If true then call the delegate supplied by the client for further clean up operations. The delegate invoked in new thread.</param>
        public void PutObject(T item, bool Release)
        {
            if (item != null && Release && _cleanUp != null)
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => CleanUp(item));
                return;
            }

            if (item == null || _objects.Count >= MaxItems)
                return;

            _objects.Add(item);
        }

        private void CleanUp(T obj)
        {
            _cleanUp(obj);
        }
    }
}
