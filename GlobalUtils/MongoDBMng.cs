﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;

namespace GlobalUtils
{
    public class MongoDBMng
    {
        MongoClient client;
        IMongoDatabase database;
        IMongoCollection<Quote> collection;
     
        public MongoDBMng(string ConnectionString, string DatabaseName)
        {
            client = new MongoClient(ConnectionString);
            database = client.GetDatabase(DatabaseName);            
        }

        public void CreateDatabase(string DatabaseName)
        {
            bool dbCreated = false;            
            IAsyncCursor<BsonDocument> curr = client.ListDatabases();
            while (curr.MoveNext())
            {
                var doc = curr.Current;
                foreach (BsonDocument bd in doc)
                {
                    if (((BsonString)bd["name"]).Value.Equals(DatabaseName))
                        dbCreated = true;
                }
            }
                        
            if (!dbCreated)
            {                
                database.CreateCollection("snapshot");
                
                var collection = database.GetCollection<GlobalUtils.Quote>("snapshot");
                CreateIndexOptions createIndexOptions = new CreateIndexOptions();
                createIndexOptions.Background = true;
                createIndexOptions.Unique = true;
                collection.Indexes.CreateOne("{ BrandName:1, SymbolName:1 }", createIndexOptions);
            }            
        }

        object obj = new object();
        bool CollectionConnected = false;
        public void ConnectCollection(string CollectionName, bool WriteConcern)
        {
            lock(obj)
            {
                if (CollectionConnected)
                    return;

                MongoCollectionSettings m = new MongoCollectionSettings();
                m.WriteConcern = new WriteConcern((WriteConcern) ? (1) : (0));

                collection = database.GetCollection<GlobalUtils.Quote>(CollectionName, m);

                CollectionConnected = true;
            }            
        }

        public bool Upsert(Quote quote)
        {                                    
            UpdateResult updateResult = collection.UpdateOne<Quote>(p => p.BrandName == quote.BrandName && p.SymbolName == quote.SymbolName, 
                Builders<Quote>.Update.Set(p => p.BrandName, quote.BrandName).Set(p => p.SymbolName, quote.SymbolName).Set(p => p.ServerTime, quote.ServerTime).Set(p => p.LocalTime, quote.LocalTime).Set(p => p.Bid, quote.Bid).Set(p => p.Ask, quote.Ask), 
                new UpdateOptions { IsUpsert = true });

            return updateResult.IsAcknowledged;
        }

        public void Insert(Quote quote)
        {
            try
            {
                collection.InsertOne(quote);
            }
            catch { }            
        }

        public string InsertWithResult(Quote quote)
        {
            string error = string.Empty;

            try
            {
                collection.InsertOne(quote);
            }
            catch (MongoWriteException mwx)
            {
                if (mwx.WriteError.Category != ServerErrorCategory.DuplicateKey)
                {
                    error = mwx.WriteError.Message;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return error;
        }

        public void Update(Quote quote)
        {
            var builder = Builders<Quote>.Filter;
            var filter = builder.Eq("BrandName", quote.BrandName) & builder.Eq("SymbolName", quote.SymbolName);

            var update = Builders<Quote>.Update.Set(p => p.ServerTime, quote.ServerTime).Set(p => p.LocalTime, quote.LocalTime).Set(p => p.Bid, quote.Bid).Set(p => p.Ask, quote.Ask);

            collection.UpdateOne(filter, update);
        }
    }
}
