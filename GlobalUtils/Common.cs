﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalUtils
{
    public class Common
    {
        public static bool IsFeedInSession(Dictionary<int, List<FeedSession>> FeedSessions)
        {
            List<FeedSession> sessions;
            if (!FeedSessions.TryGetValue((int)DateTime.Now.DayOfWeek, out sessions))
                return false;

            foreach (FeedSession fs in sessions)
            {
                if (DateTime.Now.TimeOfDay >= fs.Start && DateTime.Now.TimeOfDay <= fs.End)
                    return true;
            }

            return false;
        }
    }
}
