﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GlobalUtils;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Concurrent;
using System.Threading;

namespace GlobalUtils
{
    public class Mailer
    {
        private ObjectPool<SmtpClient> smtpObjectPool;
        private BlockingCollection<MailData> MailQ;

        private string MailSmtpServer;
        private int MailSmtpPort;
        private bool MailUseDefaultCredentials;
        private string MailUsername;
        private string MailPassword;
        private bool MailEnableSSL;
        private string MailDeliveryMethod;
        private int SmtpConnectionTimeoutMS;

        private int SendMailRetryTimes;
        private bool IgnoreCetificateValidationOnSendMail;

        public Mailer(int SmtpObjectPoolSize, string MailSmtpServer, int MailSmtpPort, bool MailUseDefaultCredentials, string MailUsername, string MailPassword, bool MailEnableSSL, string MailDeliveryMethod,
            int SmtpConnectionTimeoutMS, int SendMailRetryTimes, bool IgnoreCetificateValidationOnSendMail)
        {
            this.SendMailRetryTimes = SendMailRetryTimes;
            this.IgnoreCetificateValidationOnSendMail = IgnoreCetificateValidationOnSendMail;

            this.MailSmtpServer = MailSmtpServer;
            this.MailSmtpPort = MailSmtpPort;
            this.MailUseDefaultCredentials = MailUseDefaultCredentials;
            this.MailUsername = MailUsername;
            this.MailPassword = MailPassword;
            this.MailEnableSSL = MailEnableSSL;
            this.MailDeliveryMethod = MailDeliveryMethod;
            this.SmtpConnectionTimeoutMS = SmtpConnectionTimeoutMS;

            smtpObjectPool = new ObjectPool<SmtpClient>(ReleaseSmtpClient);
            MailQ = new BlockingCollection<MailData>();

            System.Threading.Tasks.Task.Factory.StartNew(() => Init(SmtpObjectPoolSize));                                                
        }

        private void Init(int SmtpObjectPoolSize)
        {
            //int SmtpObjectPoolSize = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpObjectPoolSize"]);
            //int SmtpObjectPoolSize = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpObjectPoolSize"]);
            for (int i = 0; i < SmtpObjectPoolSize; i++)
                smtpObjectPool.PutObject(CreateNewSmtpClient(MailSmtpServer, MailSmtpPort, MailUseDefaultCredentials, MailUsername, MailPassword, MailEnableSSL, MailDeliveryMethod,
                SmtpConnectionTimeoutMS), false);

            Thread trd = new Thread(new ThreadStart(Wait4Mails));
            trd.IsBackground = true;
            trd.Start();
        }

        /*private SmtpClient CreateNEwSmtpClient()
        {
            SmtpClient smtpClient = null;
            try
            {
                smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MailSmtpServer"]);
                smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MailSmtpPort"]);
                smtpClient.UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["MailUseDefaultCredentials"]);
                smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MailUsername"], ConfigurationManager.AppSettings["MailPassword"]);
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["MailEnableSSL"]);
                if (ConfigurationManager.AppSettings["MailDeliveryMethod"] != "None")
                {
                    if (ConfigurationManager.AppSettings["MailDeliveryMethod"] == "Network")
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                smtpClient.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpConnectionTimeoutMS"]);
            }
            catch (Exception ex)
            {
                try
                {                    
                    smtpClient.Dispose();
                }
                catch { }

                GlobalUtils.Tracer.GetTracer().Error("[Mailer] CreateNEwSmtpClient Exception: ", ex);

                smtpClient = null;
            }
            
            return smtpClient;
        }*/

        private SmtpClient CreateNewSmtpClient(string MailSmtpServer, int MailSmtpPort, bool MailUseDefaultCredentials, string MailUsername, string MailPassword, bool MailEnableSSL, string MailDeliveryMethod,
            int SmtpConnectionTimeoutMS)
        {
            SmtpClient smtpClient = null;
            try
            {
                smtpClient = new SmtpClient(MailSmtpServer);
                smtpClient.Port = MailSmtpPort;
                smtpClient.UseDefaultCredentials = MailUseDefaultCredentials;
                smtpClient.Credentials = new System.Net.NetworkCredential(MailUsername, MailPassword);
                smtpClient.EnableSsl = MailEnableSSL;
                if (MailDeliveryMethod != "None")
                {
                    if (MailDeliveryMethod == "Network")
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                smtpClient.Timeout = SmtpConnectionTimeoutMS;
            }
            catch (Exception ex)
            {
                try
                {
                    smtpClient.Dispose();
                }
                catch { }

                GlobalUtils.Tracer.GetTracer().Error("[Mailer] CreateNEwSmtpClient Exception: ", ex);

                smtpClient = null;
            }

            return smtpClient;
        }

        public SmtpClient GetNewSmtpClient()
        {
            return CreateNewSmtpClient(MailSmtpServer, MailSmtpPort, MailUseDefaultCredentials, MailUsername, MailPassword, MailEnableSSL, MailDeliveryMethod,
                SmtpConnectionTimeoutMS);
        }

        private void ReleaseSmtpClient(SmtpClient smtpClient)
        {
            if(smtpClient != null)
                smtpClient.Dispose();
        }

        public SmtpClient GetSmtpClient()
        {
            return smtpObjectPool.GetObject();
        }

        public void PutSmtpClient(SmtpClient sc)
        {
            smtpObjectPool.PutObject(sc, false);
        }

        public void SendMail(MailData md)
        {
            try
            {                
                if (!MailQ.TryAdd(md))
                {
                    if (!MailQ.TryAdd(md))
                    {
                        GlobalUtils.Tracer.GetTracer().Error("[Mailer] SendMail Fail adding mail object to blocing collection");
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalUtils.Tracer.GetTracer().Error("[Mailer] SendMail Exception: ", ex);                   
            }            
        }

        private void Wait4Mails()
        {
            foreach (MailData md in MailQ.GetConsumingEnumerable())
            {
                if (md.ResendCounter < SendMailRetryTimes)
                    System.Threading.Tasks.Task.Factory.StartNew(() => TrySendMail(md));                        
                    //TrySendMail(md);
            } 
        }

        private void TrySendMail(MailData md)
        {
            //return;
            if (IgnoreCetificateValidationOnSendMail)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    (sender, certificate, chain, sslPolicyErrors) => true;
            }

            GlobalUtils.Tracer.GetTracer().Info("[Mailer] TrySendMail start To : " + md.To);
            
            bool IsDispose = false;

            System.Net.Mail.SmtpClient smtpClient = GetSmtpClient();

            if (smtpClient == null)
            {
                smtpClient = GetNewSmtpClient();
                IsDispose = true;
            }

            if (smtpClient == null)
            {
                GlobalUtils.Tracer.GetTracer().Error("[Mailer] SendMail - Could not get SmtpClient object");
                md.ResendCounter++;
                SendMail(md);
            }
            else
            {
                try
                {
                    using (MailMessage mail = new MailMessage(new MailAddress(md.MailFromAddr, md.MailDisplayName), new MailAddress(md.To, "")))
                    {
                        if (!string.IsNullOrEmpty(md.Cc))
                        {
                            GlobalUtils.Tracer.GetTracer().Info("[Mailer] TrySendMail CC : " + md.Bcc);

                            string[] MailSentToAdditional = md.Cc.Split(',');
                            for (int i = 0; i < MailSentToAdditional.Length; i++)
                            {
                                mail.CC.Add(MailSentToAdditional[i]);                                
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(md.Bcc))
                        {
                            GlobalUtils.Tracer.GetTracer().Info("[Mailer] TrySendMail Bcc : " + md.Bcc);

                            string[] bccClients = md.Bcc.Split(',');
                            for (int i = 0; i < bccClients.Length; i++)
                            {
                                mail.Bcc.Add(bccClients[i]);
                            }
                        }

                        mail.Subject = md.Subject;

                        mail.Body = md.Body;

                        mail.IsBodyHtml = true;

                        smtpClient.Send(mail);

                        GlobalUtils.Tracer.GetTracer().Info("[Mailer] Mail to " + md.To + " send successfuly");
                    }
                }
                catch (Exception ex)
                {
                    GlobalUtils.Tracer.GetTracer().Error("[Mailer] SendMail Exception: ", ex);
                    md.ResendCounter++;
                    SendMail(md);
                }
                finally
                {
                    if (IsDispose)
                        smtpClient.Dispose();
                    else
                        PutSmtpClient(smtpClient);
                }
            }
        }

        public void SendMail(string To, string Subject, string Body, string MailFromAddr, string MailDisplayName, string MailSentToAdditionalRecepients = null)
        {
            if (IgnoreCetificateValidationOnSendMail)
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    (sender, certificate, chain, sslPolicyErrors) => true;
            }

            GlobalUtils.Tracer.GetTracer().Info("[Mailer] SendMail start for : " + To);

            bool IsDispose = false;

            System.Net.Mail.SmtpClient smtpClient = GetSmtpClient();
            if (smtpClient == null)
            {
                smtpClient = GetNewSmtpClient();
                IsDispose = true;
            }
            if (smtpClient == null)
            {
                GlobalUtils.Tracer.GetTracer().Error("[Mailer] SendMail - Could not get SmtpClient object");                
            }
            else
            {
                try
                {
                    using (MailMessage mail = new MailMessage(new MailAddress(MailFromAddr, MailDisplayName), new MailAddress(To, "")))
                    {
                        if (!string.IsNullOrEmpty(MailSentToAdditionalRecepients))
                        {
                            string[] MailSentToAdditional = MailSentToAdditionalRecepients.Split(',');
                            for (int i = 0; i < MailSentToAdditional.Length; i++)
                            {
                                mail.To.Add(MailSentToAdditional[i]);
                            }
                        }

                        mail.Subject = Subject;

                        mail.Body = Body;

                        mail.IsBodyHtml = true;

                        smtpClient.Send(mail);
                    }
                }
                catch (Exception ex)
                {
                    GlobalUtils.Tracer.GetTracer().Error("[Mailer] SendMail Exception: ", ex);                   
                }
                finally
                {
                    if (IsDispose)
                        smtpClient.Dispose();
                    else
                        PutSmtpClient(smtpClient);
                }
            }
        }



    }
}
