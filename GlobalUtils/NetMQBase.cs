﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMQ;
using NetMQ.Sockets;

namespace GlobalUtils
{
    public class NetMQBase : IDisposable
    {
        public enum SocketType
        {
            PUB = 0,
            SUB = 1
        }

        private PublisherSocket PubSocket;
        private SubscriberSocket SubSocket;

        protected NetMQBase(SocketType Type, string Address, int HighWatermark, string TopicSubscription)
        {
            PubSocket = null;

            switch ((int)Type)
            {

                case (int)SocketType.PUB:
                    {
                        PubSocket = new PublisherSocket();
                        PubSocket.Options.SendHighWatermark = HighWatermark;
                        PubSocket.Bind(Address);                        
                    }
                    break;

                case (int)SocketType.SUB:
                    {
                        SubSocket = new SubscriberSocket();
                        SubSocket.Options.ReceiveHighWatermark = HighWatermark;
                        SubSocket.Connect(Address);
                        SubSocket.Subscribe(TopicSubscription);
                    }
                    break;
            }
            
        }

        /// <summary>
        /// Disposes the instance of SocketClient.
        /// </summary>
        public void Dispose()
        {            
            try
            {
                if (PubSocket is PublisherSocket)
                {
                    PubSocket.Close();
                    PubSocket.Dispose();
                }
            }
            catch { }

        }

        protected void PublishMessage(string Msg)
        {
            PubSocket.SendFrame(Msg);
        }

        protected string ReceiveMessage()
        {
            return SubSocket.ReceiveFrameString();
        }
        
    }
}
