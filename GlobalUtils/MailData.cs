﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GlobalUtils
{
    public class MailData
    {        
        public string Subject = string.Empty;
        public string Body = string.Empty;
        public string MailFromAddr = string.Empty;
        public string MailDisplayName = string.Empty;
        public string To = string.Empty;
        public string Cc = string.Empty;
        public string Bcc = string.Empty;
        public int ResendCounter = 0;
    }
}
