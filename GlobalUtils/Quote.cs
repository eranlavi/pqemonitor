﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalUtils
{
    public class Quote
    {
        public string BrandName = string.Empty;
        public string SymbolName = string.Empty;
        public DateTime ServerTime = DateTime.Now;
        public DateTime LocalTime = DateTime.Now;
        public double Bid = 0.0;
        public double Ask = 0.0;
    }
}
